import React, { Component, Fragment } from 'react';
import { render } from 'react-dom';

export default class Footer extends Component {
    
    render() {
        
        return (
            <div className="row" id ="bottom-container">
                <div className="col-md-12">
                    <p>
                    {'\u00A9'} 2020 DPO | <a href="https://discordapp.com/invite/635JYF3">Discord Server</a>  
                    </p>
                </div>
            </div>
        )
    }
}