import React, { Component } from 'react';
import axios from 'axios';


export default class LoginForm extends Component {
    state = {
        inputUsername: "",
        inputPassword: "",
        loggedIn: false
      }

    constructor(props) {
        super();
       
    }

    loginHandler = (event) => {    
        event.preventDefault(); 
        var params = new URLSearchParams();
		params.append('User', this.state.inputUsername);
        params.append('Pwd', this.state.inputPassword);
        
        var callAction =  process.env.REACT_APP_API_ENDPOINT + 'LoginHandler.php';
        
		axios.post(callAction, params)
		.then(res => {
			const message = res.data;
            console.log(message);
        })
        .catch(function (error) {
            console.log(error);
          })
      }

    render() {
        return (
            <div className="Login">
                <form onSubmit ={(event) => this.loginHandler(event)} >
                    <div className="form-group">
                        <label htmlFor="usernameInput">Email address</label>
                        <input type="text" className="form-control" id="usernameInput"  placeholder="Enter Username" 
                         onChange= {(event) => this.setState({inputUsername: event.target.value})}/>
                    </div>

                    <div className="form-group">
                        <label htmlFor="passwordInput">Password</label>
                        <input type="password" className="form-control" id="passwordInput" placeholder="Password"
                         onChange= {(event) => this.setState({inputPassword: event.target.value})}></input><br/>
                    </div>
                    <button type="submit" className ="btn btn-primary">Login</button>
                </form>
            </div>
        )
    }
}