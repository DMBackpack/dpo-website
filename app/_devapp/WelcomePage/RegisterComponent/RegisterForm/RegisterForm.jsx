import React, { Component } from 'react';
import axios from 'axios';


export default class RegisterForm extends Component {
    state = {
        account: "",
        password: "",
        password2: "",
        email: "",
        regMessage: 0,
      }

    constructor(props) {
        super();
       
    }

    

    registerHandler = (event) => {    
        event.preventDefault(); 
        var params = new URLSearchParams();
		params.append('User', this.state.account);
        params.append('Pwd', this.state.password);
        params.append('Pwd2', this.state.password2);
        params.append('Email', this.state.email);
        
        var callAction =  process.env.REACT_APP_API_ENDPOINT + 'RegisterHandler.php';
        
		axios.post(callAction, params)
		.then(res => {
            const message = res.data;
            this.setState({regMessage: message});
        })
        .catch(function (error) {
            console.log(error);
          })
      }

    setMessage(){
        if(this.state.regMessage == 1){
            return(
                <div className="alert alert-success mt-2" role="alert">
                    Your account has been successfully registered! You can now login on the client and play.
                </div>
            )
        } 

        if(this.state.regMessage == 2){
            return(
                <div className="alert alert-warning mt-2" role="alert">
                    The two passwords do not match.
                </div>
            )
        }

        if(this.state.regMessage == 3){
            return(
                <div className="alert alert-warning mt-2" role="alert">
                    This account name exists already. Please pick another one.
                </div>
            )
        }

        if(this.state.regMessage == 4){
            return(
                <div className="alert alert-danger mt-2" role="alert">
                    Your account could not be created. Contact the administrator on Discord.
                </div>
            )
        }
    }
    render() {
        return (
            <div className="Login">
                <form onSubmit ={(event) => this.registerHandler(event)} >
                    <div className="form-group">
                            <label htmlFor="username">Username</label>
                            <input type="text" className="form-control" placeholder="Username"  
                            onChange= {(event) => this.setState({account: event.target.value})}/>
                    </div>

                    <div className="form-group">
                            <label htmlFor="password">Password</label>
                            <input type="password" className="form-control" placeholder="Password" aria-label="Password" 
                            aria-describedby="password" onChange= {(event) => this.setState({password: event.target.value})}/>
                    </div>

                    
                    <div className="form-group">
                            <label htmlFor="password2">Repeat Password</label>               
                            <input type="password" className="form-control" placeholder="Repeat Password" aria-label="Password2" 
                            aria-describedby="password2" onChange= {(event) => this.setState({password2: event.target.value})}/>
                    </div>

                    <div className="form-group">
                            <label htmlFor="email">Email</label>         
                            <input type="email" className="form-control" placeholder="Email" aria-label="Email" 
                            aria-describedby="email" onChange= {(event) => this.setState({email: event.target.value})}/>
                    </div>

                    <button type="submit" className ="btn btn-primary" >Register</button>
                </form>


                {this.setMessage()}
            </div>
        )
    }
}