import React, { Component, Fragment } from 'react';


import LoginForm from './LoginComponent/LoginForm/LoginForm';
import RegisterForm from './RegisterComponent/RegisterForm/RegisterForm';

export default class WelcomePage extends Component {
    render() {
        return (
            <div className="row" id="page-container">
                <div className="col-md-4">
                    <LoginForm />
                </div>
                <div className="col-md-8">
                    
                </div>
            </div>
        )
    }
}