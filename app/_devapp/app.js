import React, { Component, Fragment } from 'react';
import { render } from 'react-dom';
import axios from 'axios';

/* Import Components */
import Header from './Header';
import Footer from './Footer';
import WelcomePage from './WelcomePage/WelcomePage';
import HomePage from './GameHomePage/HomePageComponent/HomePageComponent';
import StatisticsComponent from './GameHomePage/StatisticsComponent/StatisticsComponent';
// import Routing from './Router/RouterMainComponent';


/* globals __webpack_public_path__ */
__webpack_public_path__ = `${window.STATIC_URL}/app/assets/bundle/`;
 
class Myapp extends Component {

    state = {
        loggedIn: true,
    }

    constructor(){
        super();
    }


    render() {

        return (
            <div className="dashboard">
                <Header />
                <StatisticsComponent />
                <HomePage />
                <Footer />
            </div> 
        )
    }
}



  

render(<Myapp/>, document.getElementById('app'));