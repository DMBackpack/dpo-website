import React, { Component, Fragment } from 'react';
import { render } from 'react-dom';

export default class Header extends Component {
    render() {
        return (
            <div className="col-md-12" id = "top-navigation">
                    <nav className="navbar navbar-expand-lg navbar-light bg-light static-top">
                        <a className="navbar-brand" href="#">Dark Pirates Online</a>				
                        <div className="nav navbar-nav flex-row float-left mr-auto">
                            <ul className ="navbar-nav navbar-left">
                                <li>Play & Test Phase </li>
                            </ul>
                        </div>
                        <div className="nav navbar-nav flex-row float-right ml-auto">                           
                            <ul className="navbar-nav navbar-right">
                                <li className="nav-item ">
                                    <a className="nav-link" href="#"></a>
                                </li>
                            </ul>
                            
                        </div>
                    </nav>                
            </div>
        )
    }
}