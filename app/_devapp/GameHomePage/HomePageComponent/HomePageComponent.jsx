import React, { Component } from 'react';
import axios from 'axios';
import RegisterForm from '../../WelcomePage/RegisterComponent/RegisterForm/RegisterForm'

export default class HomePageComponent extends Component {


	componentDidMount(){
		
	}
    render() {
		//const regForm = "Play & Test available soon! Stay tuned on Discord!";
		const regForm = <RegisterForm />;
        return (
            <div className="row" id="page-container">              
                <div className="col-md-8">
               		<div className="card bg-default mb-4">
						<h5 className="card-header">
							<i className="far fa-map"></i> 	Welcome to Dark Pirates Online's website!
						</h5>
						<div className="card-body">						
							<p className="card-text">
							Hello!	<br></br>
							<b>Play & Test</b> Phase is online, as of 20th April 2020!
							
							<br></br>			
							</p>
						</div>
						<div className="card-footer">
							<a href="https://discord.gg/635JYF3" target="_blank">
								<button type="button" className="btn btn-outline-primary">
									Join Discord!
								</button>
							
							</a> 
							<span className="ml-2">-- Stay in touch with the latest updates and chats there.</span>
						</div>
					</div> 
					
					<div className="card bg-default">
						<h5 className="card-header">
							Play & Test Phase Info
						</h5>
						<div className="card-body">
								<p className="card-text">
										<b>PLAY &amp; TEST FEATURES</b>
								
								</p>
								<ul>
									<li>
										Leveling rates x10
									</li>
									<li>
										Drop rate x10
									</li>
									<li>
										Fairy Growth x100
									</li>
								<li>
								
									When you create a character, you will also be given:
									<ul>
										<li>
											- Up to 32 slot inventory items
										</li>
									</ul>
								</li>
								<li>
									Newbie Chest now gives, in addition to everything it gave before:
									<ul><li>
											- Fairy Box
										</li>
										<li>
											- 2 Stacks x 99 Auto Rations
										</li>
										<li>
											- One stack of 99 x each Great fruit
										</li>
										<li>
											- Standard Berserk, Standard Magic, Standard Recover, Standard Protection,&nbsp;Standard Meditation
										</li>
										<li>
											- Lv. 35 Ultimate Chest -- will give a piece of unsealed gear up to lv 65
										</li>
										<li>
											- 99x Heaven's Berry
										</li>
									</ul></li>
							</ul>
						</div>
						<div className="card-footer">
							
						</div>
					</div> 
					
					<div className="card bg-default mt-4">
						<h5 className="card-header">
							PLANNED FEATURES
						</h5>
						<div className="card-body">
								<p className="card-text">
										<b>PLANNED FEATURES</b>
								
								</p>
								<ul><li>
									Resource Exchange Parties [Small groups of NPCs in each map that will be interested in giving gold for map-specific resources]
								</li>
								<li>
									Rise of the Dark Pirates [Mid-level quest chain with a final boss, option for players to get gold and gear]
								</li>
								<li>
									CA/FC/DS/DW are the same
								</li>
								<li>
									2v2/3v3/4v4/5v5 arena maps [using the already existing ones, just trying out new things for them]
								</li>
								<li>
									Exchange resources (mob drops) for 1st gen Pets, Food and Fruits
								</li>
								<li>
									[More will be posted as things are added]
								</li>
							</ul>
						</div>
						<div className="card-footer">
							
						</div>
					</div>
					
			    </div>
				<div className="col-md-4"> 
					<div className="card bg-default mb-4">
						<h5 className="card-header">
							Register an account
						</h5>
						<div className="card-body">
							{regForm}
						</div>
						<div className="card-footer">
							
						</div>
					</div> 

					<div className="card bg-default">
						<h5 className="card-header">
							Links
						</h5>
						<div className="card-body">
							<p className="card-text">
								If you have the old client, download only the patch.
							</p>
							<p>							
								<b>Client Download:</b> <a href="https://mega.nz/file/cd5ljALS#wvyeLYTlN47DpUxl-F9kB0i2c_LP6aKru1jCdvb_oh8" target="_blank"> Download DPO Client - Play & Test Phase</a>
								<br></br><i>Extract Dark Pirates Online Client in the directory you prefer, then run <b>run.bat</b> to open the client. </i>
								<br></br>
								<br></br>
								<b>New exe patch:</b> <a href="https://mega.nz/file/xYZxTKja#Y3O4u9gEQIN-bx7WDiQIdRdXxzHfvjf5ha5ntUYUREQ">New Game.exe patch</a>
								<br></br>
							</p>
						</div>
						<div className="card-footer">
							
						</div>
					</div> 
                </div>
            </div>
        )
    }
}