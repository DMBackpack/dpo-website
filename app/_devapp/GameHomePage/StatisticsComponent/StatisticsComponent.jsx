import React, { Component } from 'react';
import axios from 'axios';


export default class StatisticsComponent extends Component {
    state = {
        accountCount: 0,
        characterCount: 0,
        onlineCount: 0
      }

    constructor(props) {
        super();
       
    }

    

    getStatisticsHandler = () => {        
        var callAction =  process.env.REACT_APP_API_ENDPOINT + 'StatisticsHandler.php';
        
		axios.get(callAction,)
		.then(res => {
            const message = res.data;
            this.setState({
               ...message
            });
        })
        .catch(function (error) {
            console.log(error);
          })
      }

    componentDidMount(){
        this.getStatisticsHandler();
    }
    render() {
        return (
            <div className="row" id="page-container">
               <div className ="col-md-12">
                    <div className="alert alert-light" role="alert">
                        <center>
                            <ul className = "stats-list">
                                <li><i className="fa fa-id-card-alt"></i> Accounts: {this.state.accountCount}</li>
                                <li><i className="fa fa-users"></i> Characters: {this.state.characterCount}</li>
                                <li><i className="fa fa-desktop"></i> Online: {this.state.onlineCount}</li>
                            </ul>
                        </center>
                    </div>
               </div>
            </div>
        )
    }
}