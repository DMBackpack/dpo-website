<?php

/*
 returns:
 1 -- acc ok
 2 -- passwords dont match
 3 -- acc name exists
 4 -- other problem; contact admin
*/
session_start();
include("../php_config/Database.php");


class RegisterHandler extends Database {

    private $Username;
    private $Password;
    private $Email;
    private $QueryResult;

    function __construct($Username, $Password, $Email){
        try{
            parent::__construct();
            $this->Username = $Username;
            $this->Password = $Password;
            $this->Email = $Email;

            $this->QueryResult = new stdClass();
        }
        catch(PDOException $e){
            print_r($e);
        }
    }
   
    function CheckIfUserExists(){
      
        $checkUserExistenceQuery = "SELECT * FROM [dbo].[account_login] WHERE name LIKE ?";
        $checkUserExistenceParams = array($this->Username);

        $queryExec = sqlsrv_query($this->AccDBConnection, $checkUserExistenceQuery, $checkUserExistenceParams);
        
        if($queryExec == false){
            die(4);
        }

        if(sqlsrv_has_rows($queryExec)){
            return true;
        }      

        return false;
    }

    function RegisterUser(){
        $registerUserQuery = "INSERT INTO [dbo].[account_login] (name, password, originalPassword, email, ban)
        VALUES (?, ?, ?, ?, 0);";

        $registerUserQueryParams = array($this->Username,
                                        strtoupper(md5($this->Password)),
                                        "not saving this",
                                        $this->Email);
        $queryExec = sqlsrv_query($this->AccDBConnection, $registerUserQuery, $registerUserQueryParams);
        
        if($queryExec == false){
            die(4);
            return false;
        }

        return true;
    }
}

if(!isset($_POST)){
    echo "Something went wrong. :(";
}

else{
    try{
        if($_POST["Pwd"] != $_POST["Pwd2"]){
            echo 2;
            return;
        }
        $RegisterAction = new RegisterHandler($_POST["User"], $_POST["Pwd"], $_POST["Email"]);
        if($RegisterAction->CheckIfUserExists()){
            echo 3;
            return;
        }

        if($RegisterAction->RegisterUser()){
            echo 1;
            return;
        }
    }
    catch(Exception $e){
        echo 4;
    }
}
?>