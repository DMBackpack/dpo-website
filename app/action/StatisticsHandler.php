<?php

/*
 returns:
 1 -- acc ok
 2 -- passwords dont match
 3 -- acc name exists
 4 -- other problem; contact admin
*/
session_start();
include("../php_config/Database.php");


class GameStatistics extends Database {

    private $ServerStatistics; 
    function __construct(){
        try{
            parent::__construct();
        
        }
        catch(PDOException $e){
            print_r($e);
        }
    }

    function GetServerStatistics(){
        $getServerStatsQuery = "SELECT 
        ( SELECT COUNT(*) FROM [accountserver].[dbo].[account_login] ) AS 'accountCount',
        ( SELECT COUNT(*) FROM [gamedb].[dbo].[character] ) AS 'characterCount',
        ( SELECT COUNT(*) FROM [gamedb].[dbo].[character] WHERE [mem_addr] > '0' ) AS 'onlineCount'";

        $queryExec = sqlsrv_query($this->UniversalConnection, $getServerStatsQuery);
        if(!$queryExec){
            die(0);
            return;
        }
        $this->ServerStatistics = sqlsrv_fetch_object($queryExec);

        echo(json_encode(($this->ServerStatistics)));
    }

   
}

$Statistics = new GameStatistics();
$Statistics->GetServerStatistics();



?>