<?php
session_start();
include("../php_config/Database.php");
include("../php_functionality/user_functionality/UserMain.php");
include("../php_functionality/user_functionality/character_functionality/CharacterMain.php");

if(!isset($_GET['action'])){
    echo "Something went wrong. :(";
}
else{
    $ActionCallHandler = new GetActionHandler($_GET['action']);
    $ActionCallHandler->ProcessActionCall();
    $ActionCallHandler->OutputResult();
}

class GetActionHandler{

    private $ActionCall;
    private $ActionCallResult;

    function __construct($actionCall){
        if($actionCall != "" || $actionCall != null){  
              $this->ActionCall = $actionCall;
              $this->ActionCallResult = new stdClass();
         
        }
        
    }
   
    function ProcessActionCall(){
        switch($this->ActionCall){
            case "test":
                self::TestActionCall();
                break;
            case "getUser":
                self::GetUserCall();
                break;
            case "getCharacter":
                self::GetCharacterCall();
                break;

        }
    }

    function OutputResult(){
        echo json_encode($this->ActionCallResult);
    }

    private function GetUserCall(){
        $UserDetails = new UserMain($_SESSION["LoggedInUserId"]);
        $this->ActionCallResult = $UserDetails->getUserDetails();
    }

    private function GetCharacterCall(){
        if(isset($_SESSION["LoggedInUserId"])){
            $CharacterDetails = new CharacterMain($_SESSION["LoggedInUserId"]);
            $this->ActionCallResult = $CharacterDetails->getCharacterDetails();
        } else {
            $this->ActionCallResult = "0";
        }
    }

    private function TestActionCall(){
        $testObj = new stdClass();
        $testObj->text = $this->ActionCall;
        $this->ActionCallResult = $testObj;
    }

}

/*
var callAction = process.env.REACT_APP_API_ENDPOINT + 'GetActionHandler.php?action=getUser'; 
		console.log(callAction);
		axios.post(callAction)
		.then(res => {
			console.log(res.data[0]);
			const returnData = res.data[0];
			this.setState({
				Username: returnData.Username
			});
			console.log(this.state);
		})
*/
?>