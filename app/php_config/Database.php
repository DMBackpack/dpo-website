<?php
class Database{
	protected $AccDBConnection;
	protected $GameDBConnection;
	protected $UniversalConnection;
	function __construct(){
        
		$DBServer = "localhost";

		$accDBName = "accountserver";
        $accDBUser = "sa";
		$accDBPassword = "";


		$gameDBName = "gamedb";
		$gameDBUser ="sa";
		$gameDBPassword ="";

		$universalUser = "sa";
		$universalPassword = "";


		
	
       try{
			$accConectionInfo = array( "Database"=> $accDBName,
									"UID"=> $accDBUser,
									"PWD"=> $accDBPassword);

			$gameConectionInfo = array( "Database"=> $gameDBName,
									"UID"=> $gameDBUser,
									"PWD"=> $gameDBPassword);
		
			$universalConnectionInfo = array( "UID" =>$universalUser,
											"PWD" => $universalPassword );

			$this->AccDBConnection = sqlsrv_connect( $DBServer, $accConectionInfo );
			if(!$this->AccDBConnection){
				die("Connection to AccountServer database couldn't be established.");
			}

			$this->GameDBConnection = sqlsrv_connect( $DBServer, $gameConectionInfo );
			if(!$this->GameDBConnection){
				die("Connection to GameDB database couldn't be established.");
			}

			$this->UniversalConnection = sqlsrv_connect( $DBServer, $universalConnectionInfo );
			if(!$this->UniversalConnection){
				die("Not working :/");
			}


			}
		catch (PDOException $e){
			$ErrorMessage = $e->getMessage();     
			// echo $ErrorMessage;   
        }
	}
    
    function GetConnection(){
        return $this->DBConnection;
    }
}   