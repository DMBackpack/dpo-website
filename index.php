<?php 

require_once("./app/php_functionality/Main.php");

$Main = new Main();

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Dark Pirates - Online</title>

    <meta name="description" content="">

    <link href="./app/assets/css/bootstrap/bootstrap.min.css" rel="stylesheet">
	  <link href="./app/assets/css/lux/bootstrap.min.css" rel = "stylesheet">
    <link href="./app/assets/fontawesome/css/all.css" rel = "stylesheet">
    <link href="./app/assets/css/custom/style.css" rel = "stylesheet">
    
    <script type="text/javascript">

    </script>

  </head>
  <body>
      <div class="container-fluid">
        <div id="app"></div>
      </div>
      <script type="text/javascript" src="./app/assets/bundle/main.bundle.js" charset=”utf-8"></script>
  </body>
</html>
